#!/bin/sh -e

# BLUE_GREEN_ENV=$(kubectl get deploy parking-lot -o json | jq -r '.metadata.labels.env' || '')
# BLUE_GREEN_ENV=''
BLUE_GREEN_ENABLED=false
printf "Current Activate BLUE_GREEN_ENV ==== $BLUE_GREEN_ENV \n"
printf "BLUE_GREEN_ENABLED ====== $BLUE_GREEN_ENABLED \n"

if [[ "$BLUE_GREEN_ENABLED" == true ]]; then
    if [[ -z "$BLUE_GREEN_ENV" || "$BLUE_GREEN_ENV" == "green" ]]; then
        printf "deploying the application to 'blue' env. \n"
        # kubectl apply -f deployment-blue.yml
        # kubectl delete -f deployment-green.yaml
        export BLUE_GREEN_ENV=blue
    else
        printf "deploying the application to 'green' env. \n"
        # kubectl apply -f deployment-green.yml
        # kubectl delete -f deployment-blue.yaml
        export BLUE_GREEN_ENV=green
    fi
else
    printf "BLUE_GREEN_DEPLOYMENT is set to false. \n"
    if [[ -z "$BLUE_GREEN_ENV" ]]; then
        export BLUE_GREEN_ENV=blue
    else
        export BLUE_GREEN_ENV=$BLUE_GREEN_ENV
    fi
    printf "deploying the application to $BLUE_GREEN_ENV env. \n"
fi
