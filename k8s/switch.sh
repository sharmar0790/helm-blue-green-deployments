#!/bin/sh -e

BLUE_GREEN_ENV=$(kubectl get deploy parking-lot -o json | jq -r '.metadata.labels.env' || '')
printf "Current Activate BLUE_GREEN_ENV ==== $BLUE_GREEN_ENV \n"
if [[ -z "$BLUE_GREEN_ENV" || "$BLUE_GREEN_ENV" == "green" ]]; then
    printf "deploying for env - blue \n"
    kubectl apply -f deployment-blue.yml
    # kubectl delete -f deployment-green.yaml
else
    printf "deploying for env - green \n"
    kubectl apply -f deployment-green.yml
    # kubectl delete -f deployment-blue.yaml
fi