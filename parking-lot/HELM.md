# HELM Commands

- helm create <`chart name`>  
  `helm create demo`

  The Helm chart directory contains:
    - Directory charts – Used for adding dependent charts. Empty by default.
    - Directory templates – Configuration files that deploy in the cluster.
    - YAML file – Outline of the Helm chart structure.
    - YAML file – Formatting information for configuring the chart.
- helm lint <`chart directory`>
  - This command takes a path to a chart and runs a series of tests to verify that
    the chart is well-formed.  
- To verify/test your charts, run the below command and check your generated yamls before installing.   
   `helm install --dry-run --debug ./demo --generate-name`

- Testing command  
  `helm template test argo --debug`
