package com.parking.constants;

public final class ApplicationConstants {
    public static final String SLASH = "/";
    public static final String V1 = "/v1";
    public static final String V2 = "/v2";

    private ApplicationConstants() {
    }
}
