# GITLAB Setup on Mac

## Steps

- brew install gitlab-runner
- gitlab-runner register
- brew services start gitlab-runner
- brew services stop gitlab-runner

if needed make/update the changes for a runner to pick untagged jobs.

⚡ kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d