#!/bin/sh

echo '##########################################'
echo 'Configuring and Installing OpenShift Cluster'
echo '##########################################'
crc cleanup
sleep 5

echo '##########################################'
echo 'SetUp Started'
echo '##########################################'
crc setup
sleep 5

echo '##########################################'
echo 'Starting CRC'
echo '##########################################'
crc start -p /Users/rv0790/Documents/projects/openshift/pull-secret.txt
sleep 5

ret=$?
if [ $ret -ne 0 ]; then
exit $ret
fi